package com.warion.autohub.Model;

import android.location.Location;

/**
 * Created by grahamherceg on 2/8/18.
 */

public interface CompletionBlock {
    public void returnLocation(Location location);
}
