package com.warion.autohub.fragments.home;

public interface HomeContract {

    interface view {

        void showInvalidInputsToast();
        void startDrive();
        void stopDrive();
    }

}
